function output = k_means_f(selection, k)
    [height,width,deep] = size(selection);
    frame_size = height*width;
    [x,y] = meshgrid(0:height-1, 0:width-1);
    X = cat(3,selection,x,y);
    X = reshape(X,frame_size,deep+2);
    m = mean(X,1);
    s = std(X,[],1);
    XN = (X-m(ones(1,frame_size),:))./s(ones(1,frame_size),:);
    opts = statset('MaxIter',1000);
    labels = kmeans(XN,k, 'EmptyAction', 'singleton', 'Replicates', 5, 'Options', opts);
    output = reshape(labels, height, width, 1);
end