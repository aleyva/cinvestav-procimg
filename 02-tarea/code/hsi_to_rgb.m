function rgb = hsi_to_rgb(H, S, I)    

    [height, width, depth] = size(H);

    H = H * 2 * pi;
    
    % crear las nuevas matrices
    R = zeros(height, width);
    G = zeros(height, width);
    B = zeros(height, width);

    % RG sector (0 <= H < 2*pi/3).
    idx = find( (0 <= H) & (H < 2*pi/3));
    B(idx) = I(idx) .* (1 - S(idx));
    R(idx) = I(idx) .* (1 + S(idx) .* cos(H(idx)) ./ cos(pi/3 - H(idx)));
    G(idx) = 3*I(idx) - (R(idx) + B(idx));

    % BG sector (2*pi/3 <= H < 4*pi/3).
    idx = find( (2*pi/3 <= H) & (H < 4*pi/3) );
    R(idx) = I(idx) .* (1 - S(idx));
    G(idx) = I(idx) .* (1 + S(idx) .* cos(H(idx) - 2*pi/3) ./ cos(pi - H(idx)));
    B(idx) = 3*I(idx) - (R(idx) + G(idx));

    % BR sector.
    idx = find( (4*pi/3 <= H) & (H <= 2*pi));
    G(idx) = I(idx) .* (1 - S(idx));
    B(idx) = I(idx) .* (1 + S(idx) .* cos(H(idx) - 4*pi/3) ./ cos(5*pi/3 - H(idx)));
    R(idx) = 3*I(idx) - (G(idx) + B(idx));

    % Combinar los 3 canales    
    rgb = cat(3, R, G, B);
    
end