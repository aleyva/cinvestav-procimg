function hist = get_histogram(img)
    [height, width, depth] = size(img);
    
    L = 256;
    hist = zeros(L,1);    
    
    % obtener el histograma de la imgn
    for i=1:height
        for j=1:width
            value = img(i,j)+1;
            hist(value) = hist(value) + 1;
        end    
    end
    
end

