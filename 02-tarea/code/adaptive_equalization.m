function img_ad_eq = adaptive_equalization(img_gray, clip_level, context_reg)
    
    [height, width, depth] = size(img_gray);        
    
    % obteniendo el alto y ancho en pixeles para cada region contextual.
    height_cont_reg = height/context_reg(1);
    width_cont_reg = width/context_reg(2);
    matrix_cum_func = cell(context_reg(1), context_reg(2));
    matrix_context_reg = cell(context_reg(1), context_reg(2));    
    
    % calcula las funciones de distribucion acumulativa
    for i=1:context_reg(1)
        for j=1:context_reg(2)
            % calcula los puntos inicio en m y n de la region
            m1 = round( ( (i-1)*height_cont_reg ) +1 );
            n1 = round( ( (j-1)*width_cont_reg ) +1 ) ;
            % calcula los puntos finales de m y n de la region
            m2 = round( i*height_cont_reg );
            n2 = round( j*width_cont_reg );
            
            matrix_context_reg{i,j} = [m1, m2, n1, n2];            
            matrix_cum_func{i,j} = cumulative_func_by_region(img_gray, clip_level);
        end
    end
    
    % equalizacion adaptativa
    img_ad_eq = img_gray;
    for i=1:context_reg(1)
        for j=1:context_reg(2)
            mid_m = round( (matrix_context_reg{i,j}(2) - matrix_context_reg{i,j}(1) ) / 2 );
            mid_n = round( (matrix_context_reg{i,j}(4) - matrix_context_reg{i,j}(3) ) / 2 );
            for k=matrix_context_reg{i,j}(1):matrix_context_reg{i,j}(2)
                for l=matrix_context_reg{i,j}(3):matrix_context_reg{i,j}(4)
                    if(k == 251 && l == 251)
                        continue;
                    end
                    
                    % distancia en porcentaje para realizar interpolacion
                    p_m1 = abs(mid_m - (k - matrix_context_reg{i,j}(1) ) ) / height_cont_reg;
                    p_m2 = 1 - p_m1; % 1-y
                    p_n1 = abs(mid_n - (l - matrix_context_reg{i,j}(3) ) ) / width_cont_reg;                    
                    p_n2 = 1 - p_n1; % 1-x
                    
                    % calcula que regiones contextuales debe tomar
                    if( k - matrix_context_reg{i,j}(1) < mid_m)
                        cua_i = -1;
                    else
                        cua_i = 1;
                    end
                    if( l - matrix_context_reg{i,j}(3) < mid_n)
                        cua_j = -1;
                    else
                        cua_j = 1;
                    end
                    
                    % arregla puntos que no tienen las 4 regiones
                    % contextuales
                    if( i + cua_i < 1 || i + cua_i > context_reg(1))
                        cua_i = 0;
                    end                    
                    if( j + cua_j < 1 || j + cua_j > context_reg(2))
                        cua_j = 0;
                    end
                    
                    % obtiene las 4 funciones de distribucion aculumativa
                    ta = matrix_cum_func{i+cua_i, j+cua_j};                    
                    tb = matrix_cum_func{i+cua_i, j};
                    tc = matrix_cum_func{i, j+cua_j};                    
                    td = matrix_cum_func{i, j};
                    r = img_gray(k,l)+1;
                    
                    % calcula el valor del punto
                    s = p_m2 * (p_n2*ta(r) + p_n1*tb(r)) + p_m1 * (p_n2*tc(r) + p_n1*td(r));
                    img_ad_eq(k,l) = round(s);                    
                    
                end
            end
        end
    end
end