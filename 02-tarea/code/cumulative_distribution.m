function [cum_distribution, r_min, r_max] = cumulative_distribution(hist_norm, p_min, p_max)
    
    L = 256;
    cum_distribution = zeros(L,1); 
    
    %obtener la distribucion acumulada, junto con sus valores min y max
    cum_distribution(1) = hist_norm(1);
    r_min = 0;
    r_max = 0;    
    for i=2:L
        cum_distribution(i) = cum_distribution(i-1)+hist_norm(i);
        if(r_min == 0 && cum_distribution(i) >= p_min)
            r_min = i-1;
        end
        
        if(r_max == 0 && cum_distribution(i) >= p_max)
            r_max = i-1;
        end
    end
     
    %estirar los niveles de intensidad para cubrir el rango 
%     hist_eq = zeros(L,1);    
%     temp = r_max-r_min;
%     for i=1:L
%         if(cum_distribution(i) > 0)
%             s = round(255*( (i-r_min) / temp));
%             if( s >= 0 && s <= L)
%                 hist_eq(s+1) = hist_norm(i);
%             end
%         end
%     end    
    
end