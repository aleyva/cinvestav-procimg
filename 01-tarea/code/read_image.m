function [img, height, width] = read_img(img_path)
    img = mat2gray(imread(img_path));   %read img and convert to gray 
    img = padarray(img,[1 1],'both');   %add padding of 1 pixel
    [height, width, planes] = size(img);  % get the height and width of the img
end