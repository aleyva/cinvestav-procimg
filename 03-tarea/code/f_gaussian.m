function filtered_img = f_gaussian(img,D,d0)
    
    %create the filter    
    dividend = 2*d0^2;
    H = exp(-D.^2/dividend);            

    % apply the fast fourier transform to img    
    [height, width] = size(img);    
    fft_i = fft2(img,height,width);
    fft_i = fftshift(fft_i);    
    
    % apply the filter to fft_i
    im_h = H.*fft_i;
    
    % apply the fast fourier transform to im_h
    im_f = ifftshift(im_h);    
    im_n = abs(ifft2(im_f));    
    filtered_img = uint8(im_n);
end