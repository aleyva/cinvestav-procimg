function imshow_filters_cell(filters)
    monitor_dimension = get(0, 'MonitorPositions');
    figure_position = [monitor_dimension(3)*.1,monitor_dimension(4)*.1,monitor_dimension(3)*.8,monitor_dimension(4)*.8];
    figure('position',figure_position);   
    % title('Gabor filters');    
    
    [height, width, deep] = size(filters);
    for i = 1:height
        for j = 1:width
            if(i == 1 && j == 1)
                subaxis(height,width,1,'SH',0,'SV',0,'M',0,'PT',0);
            else
                subaxis((i-1)*width+j);
            end
            imshow(filters{i,j},[]); 
        end
    end    
end