function cum_dist = cumulative_func_by_region(img_gray, cliplevel)        
    
    [height, width, depth] = size(img_gray);    
    hist = get_histogram(img_gray);          
    hist_norm = hist / (height * width);    
        
    L = 256;
    exceed = zeros(L,1);
    exceed_pixels = 0;
    hist_clip = hist_norm;    
    % figure, bar(hist_clip);
    % title('Histograma normalizado');
    
    
    % calcular donde hacer clipping
    for i=1:L
        if(hist_norm(i) >= cliplevel)
            exceed(i) = 1;
            exceed_pixels = exceed_pixels + ( hist_norm(i) - cliplevel );
            hist_clip(i) = cliplevel;
        end
    end
    % figure, bar(hist_clip);    
    
    dist = exceed_pixels / ( L - sum(exceed) ) ;
    
    % agrega pixeles a la parte que no hizo clipping
    for i=1:L
        if(exceed(i) == 0)            
            hist_clip(i) = hist_clip(i) + dist;
        end
    end
    hist_clip = hist_clip * (L-1);    
    % figure, bar(hist_clip);    
    
    % obtener la distribucion acumulativa, junto con los valores min y max    
    [cum_dist, r_min, r_max] = cumulative_distribution(hist_clip, 0.05, 0.95);       
    % figure, plot(cum_dist);
    % title('Distribucion acumulativa'), xlabel('Rango'), ylabel('Intensidad'), xlim([0 256])
    
%     % ecualizacion de la imagen    
%     img_clip = img_gray;    
%     for i=1:height
%         for j=1:width
%             img_clip(i,j) = cum_dist(img_gray(i,j));            
%         end    
%     end
%     figure, imshow(img_clip); 
%     title('imagen mapeada')
    
end