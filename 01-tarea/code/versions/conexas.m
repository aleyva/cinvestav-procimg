
clear;

%ruta de las imagenes
path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01';
% patron para elegir archivos
files = dir(fullfile(path,'*.png')); 


% for file = 3:numel(files)    
%     image_path = fullfile(path,files(file).name);            
    image_path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01/facebook.png';    
    image = mat2gray(imread(image_path));   %read image and convert to gray 
    image = padarray(image,[1 1],'both');   %add padding of 1 pixel
    [height, width, planes] = size(image);  % get the height and width of the image
    
    if (height > width)
        n = height;
    else
        n = width;
    end
    eq_rel = zeros(n, n);
   
    % Algoritmo de etiquetado de imagenes conexas para 4-adyacencia        
    etiqueta = 0;
    lp = 0;
    lq = 0;
    for i = 2:height-1
        for j = 2:width-1
            if image(i, j) == 1
                lp = image(i - 1, j);
                lq = image(i, j - 1);
                if lp == 0 && lq == 0                                
                    etiqueta = etiqueta + 1;
                    image(i, j) = etiqueta;
                elseif (lp ~= lq) && (lp ~= 0) && (lq ~= 0)                    
                    eq_rel(lp, lq) = 1;
                    %Registrar equivalencia (lp, lq)
%                     if lq < lp
%                         lp = lq;
%                     end
                    %etiqueta = lp;
                    image(i, j) = lp;                    
                elseif lq ~= 0
                    image(i, j) = lq;
                elseif lp ~= 0
                    image(i, j) = lp;                
                end
            end
            %image(i, j) = lx;
        end
    end
    
    imshow(image, [0 etiqueta])
    b = bitor( bitor(eq_rel, transpose(eq_rel)),  eye(n));
    
    % Algoritmo para encontrar relaciones de equivalencias (warshall)    
    bm = eq_rel;
    for k = 1:n
        for i = 1:n
            for j = 1:n
                bm(i, j) = bitor(bm(i, j), bitand(bm(i, k), bm(k, j)) );
            end
        end
    end
    
    imshow(bm);
% end



% 3. Escribir un programa que extraiga de manera individual los contornos de varias regiones
% conexas usando los programas de los puntos 1 y 2.
% 4. Redactar un reporte de la práctica con al menos los siguientes elementos: Introducción,
% Métodos, Resultados, Conclusiones y Referencias.
% 5. Enviar los códigos fuente en Matlab tareas.tamps.cinvestav@gmail.com
