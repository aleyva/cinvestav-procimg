function [descriptor] = fourier_descriptor(file,p_fourier)

    print_val = 0;
    
%     p_fourier = 50;   
%     name_file = 'guitar';
%     file = './datos/apple/apple-1.gif';        
%     file = './datos/bell/bell-1.gif';   
%     file = './datos/bird/bird-1.gif';   
%     file = './datos/bottle/bottle-1.gif';   
%     file = './datos/crown/crown-1.gif';   
%     file = './datos/cup/cup-1.gif';   
%     file = './datos/guitar/guitar-1.gif';   
%     file = './datos/horseshoe/horseshoe-1.gif';   
%     file = './datos/key/key-1.gif';   
%     file = './datos/turtle/turtle-1.gif';           

        
    % get the boundary points of the image.
    boundary_pts = boundary_points(file);
    
    % s has a list of imaginary abers, coming from the edge points 
    s = complex(boundary_pts(:,1),boundary_pts(:,2));
    im_f = fft(s);
    im_f = fftshift(im_f);     
    
    middlepos = round(size(im_f)/2); 
    middlepos = middlepos(1); 
    rightzero=(middlepos+p_fourier+1);
    newa=zeros(size(im_f));
    newa(middlepos-p_fourier:middlepos+p_fourier)=im_f(middlepos-p_fourier:middlepos+p_fourier); 
    im_f(rightzero:numel(im_f)) = 0;
    im_f(1:middlepos-p_fourier-1)=0; 

    center_a = round(numel(im_f)/2); 
    % Coefficient normalization for translation    
    im_f(center_a)=0;     
    im_f2 = im_f./im_f(center_a+1); % im_f(1) in slides = im_f(2) for me because matlab has no 0-indexing.     
    % Coefficient normalization for rotation
    im_f2 = abs(im_f2); 
    
    descriptor = im_f2(middlepos-p_fourier:middlepos+p_fourier); 
        
    if(print_val)             
        % showing results
        monitor_dimension = get(0, 'MonitorPositions');
        figure_position = [monitor_dimension(3)*.1,monitor_dimension(4)*.1,monitor_dimension(3)*.8,monitor_dimension(4)*.8];        
        f = figure('position',figure_position);            
        subaxis(2,3,1,'SH',0,'SV',0,'M',0,'PT',.03, 'PB',.03, 'PR',.015, 'PL',.015);
        imshow(imread(file),[]);
        title('Original image');  
        
        subaxis(2);         
        f=scatter(boundary_pts(:,2),boundary_pts(:,1));
        f.Marker = '.';        
        axis equal; 
        set(gca, 'YDir','reverse')        
        title('Boundary Points');
        
        subaxis(3); 
        [total_edge_pts,~] = size(boundary_pts);
        magnitude = abs(im_f); 
        plotpts = (-total_edge_pts/2+1:1:total_edge_pts/2)';                 
        semilogy(plotpts',magnitude); 
        xlabel('Frecuency')
        ylabel('Magnitude') 
        title(sprintf('Frecuency and magnitude, points:%d',p_fourier));        
        
        subaxis(4); 
        plot(plotpts',magnitude); 
        title(sprintf('Plot Fourier descriptor, points:%d',p_fourier));
        
        subaxis(5); 
        im_f2 = ifftshift(im_f2);
        reconstructed = ifft(im_f2);           
        f=scatter(imag(reconstructed(:)),real(reconstructed(:)));
        f.Marker = '.';
        title('Reconstructed');
        axis equal; 
        set(gca, 'YDir','reverse');
        title(sprintf('Reconstructed'));
                
        saveas(f, fullfile('./results',sprintf('%s_%d.png',name_file,p_fourier)));
    end 
    
% end 