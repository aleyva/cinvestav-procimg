
clear;

%ruta de las imagenes
path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01';
% patron para elegir archivos
files = dir(fullfile(path,'*.png')); 


% for file = 3:numel(files)    
%     image_path = fullfile(path,files(file).name);            
%     image_path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01/fruit.png';        
    image_path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01/planes.png';    
    image = mat2gray(imread(image_path));   %read image and convert to gray 
    image = padarray(image,[1 1],'both');   %add padding of 1 pixel
    [height, width, planes] = size(image);  % get the height and width of the image
    
    n = 5000;
    eq_rel = zeros(n, n); %creacion arbitraria de matriz 1000x1000
   
    % Algoritmo de labeldo de imagenes conexas para 4-adyacencia        
    label = 0;    
    lp = 0;
    lq = 0;
    for i = 2:height-1
        for j = 2:width-1
            if image(i, j) == 1
                lp = image(i - 1, j);
                lq = image(i, j - 1);
                if lp == 0 && lq == 0                                
                    label = label + 1;                    
                    image(i, j) = label;
                elseif (lp ~= lq) && (lp ~= 0) && (lq ~= 0)                                        
                    eq_rel(lp, lq) = 1; %Registrar equivalencia (lp, lq)
                    image(i, j) = lp;                    
                elseif lq ~= 0
                    image(i, j) = lq;
                elseif lp ~= 0
                    image(i, j) = lp;                
                end
            end
            %image(i, j) = lx;
        end
    end
    
    %imshow(image, [0 label]);

    % Hacer "eq_rel" reflexiva, simétrica y transitiva
    bm = bitor( bitor(eq_rel, transpose(eq_rel)),  eye(n));
    
    % Algoritmo para encontrar relaciones de equivalencias (warshall)    
    max_label = label;
    for k = 1:max_label
        for i = 1:max_label
            for j = 1:max_label % for j = i:max_label
                if( bm(i, k) == 1 && bm(k, j) == 1)
                    bm(i, j) = 1;
                end
                %bm(i, j) = bitor(bm(i, j), bitand(bm(i, k), bm(k, j)) );
            end
        end
    end
    
    % Algoritmo para clases de equivalencia
    classes = unique(bm(1:max_label,1:max_label),'rows');
    image_t = image;
    new_label = max_label +1;
    for i = 1:size(classes,1)
        class = find(classes(i,:)==1); % por fila
        for k = 1:length(class)
            image_t(image_t==class(k)) = new_label;
        end
        new_label = new_label +1;
    end
    
    % esta parte no es necearia
    image_t(image_t == 0) = max_label;
    image_t = image_t - max_label;
    %imshow(image_t, [0 size(classes,1)]); %mostrar la imagen
    
    total_labels = new_label - max_label - 1;
    image_rgb = label2rgb(image_t);
    imshow(image_rgb);
    
    


% 3. Escribir un programa que extraiga de manera individual los contornos de varias regiones
% conexas usando los programas de los puntos 1 y 2.
% 4. Redactar un reporte de la práctica con al menos los siguientes elementos: Introducción,
% Métodos, Resultados, Conclusiones y Referencias.
% 5. Enviar los códigos fuente en Matlab tareas.tamps.cinvestav@gmail.com
