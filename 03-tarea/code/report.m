clear all;
close all;

%ruta de las imagenes
path_noisy = './img/noisy';
path_original = './img/original';
% patron para elegir archivos
files_noisy = dir(fullfile(path_noisy,'*.tif')); 
files_original = dir(fullfile(path_original,'*.tif'));

num_img = 6; %imagenes del 1 al 6


img_path = fullfile(path_noisy,files_noisy(num_img).name);                
img_path_o = fullfile(path_original,files_original(num_img).name);                    

% reading images
tiff_obj = Tiff(img_path,'r');    
img = tiff_obj.read(); 
close(tiff_obj);       
tiff_obj = Tiff(img_path_o,'r');    
img_o = tiff_obj.read(); 
close(tiff_obj);

% get the filter D
D = filter_D(img);    

im_nr_butter = f_butterworth(img,D,100,1);    
im_nr_gaussian = f_gaussian(img,D,100);
im_final = hf_butterworth(im_nr_butter,D,200,1,.6,.3);

monitor_dimension = get(0, 'MonitorPositions');
figure_position = [monitor_dimension(3)*.1,monitor_dimension(4)*.1,monitor_dimension(3)*.8,monitor_dimension(4)*.8];

% gaussian filter
if(0)
    figure('position',figure_position);
    subaxis(2,3,1,'SH',0,'SV',0,'M',0,'PT',.02);
    imshow(img); title(sprintf('Original %s',files_noisy(num_img).name));
    step = 20;
    for i=1:5
        subaxis(i+1);                 
        imshow(f_gaussian(img,D,i*step)); 
        title(sprintf('Lowpass filter Gaussian d0=%d',i*step));
    end
end

% butterworth filter
if(0)
    figure('position',figure_position);
    subaxis(2,3,1,'SH',0,'SV',0,'M',0,'PT',.02);
    imshow(img,[]); title(sprintf('Original %s',files_noisy(num_img).name));
    step = 20;
    for i=1:5
        subaxis(i+1);                 
        imshow(f_butterworth(img,D,i*step,1)); 
        title(sprintf('Lowpass filter Butterworth d0=%d n=1 ',i*step));
    end
end

% butterworth filter
if(0)
    figure('position',figure_position);
    subaxis(2,3,1,'SH',0,'SV',0,'M',0,'PT',.02);
    imshow(img,[]); title(sprintf('Original %s',files_noisy(num_img).name));
    step = 10;    
    for i=1:5
        subaxis(i+1);     
        % butterworth(img,d0,n)
        imshow(f_butterworth(img,D,100,i*step)); 
        title(sprintf('Lowpass filter Butterworth d0=100 n=%d ',i*step));
    end
end    

% homomorphic butterworth filter
if(0)
    figure('position',figure_position);
    subaxis(2,3,1,'SH',0,'SV',0,'M',0,'PT',.02);
    imshow(im_nr_butter,[]); title('Original filter butterworth');            
    d0 = 100;
    alpha = 0;
    beta = .05;
    step = .4;
    for i=1:5
        subaxis(i+1);                 
        imshow(hf_butterworth(im_nr_butter,D,100,1,alpha+(i*step),beta)); 
        title(sprintf('H. F. B. d0=%d n=1 alpha=%3.2f beta=%3.2f',d0,alpha+(i*step),beta));
    end
end

% homomorphic butterworth filter
if(1)
    figure('position',figure_position);
    subaxis(2,3,1,'SH',0,'SV',0,'M',0,'PT',.02);
    imshow(im_nr_butter,[]); title('Original filter butterworth');            
    d0 = 100;
    alpha = .1;
    beta = 0;
    step = .4;
    for i=1:5
        subaxis(i+1);                 
        imshow(hf_butterworth(im_nr_butter,D,100,1,alpha,beta+(i*step))); 
        title(sprintf('H. F. B. d0=%d n=1 alpha=%3.2f beta=%3.2f',d0,alpha,beta+(i*step)));
    end
end    
    
    