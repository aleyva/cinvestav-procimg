function img_eq = image_equalization(img_gray, p_min, p_max)
    
    [height, width, depth] = size(img_gray);

    % obtener el histograma de la imagen
    hist = get_histogram(img_gray);    
    hist_norm = hist / (height * width);
    % figure, bar(hist_norm);
    % title('Histograma normalizado'), xlabel('Rango'), ylabel('Intensidad'), xlim([0 256])
    
    % obtener la distribucion acumulativa, junto con los valores min y max
    [cum_dist, r_min, r_max] = cumulative_distribution(hist_norm, p_min, p_max);       
    % figure, bar(cum_dist);
    % title('Distribucion acumulativa'), xlabel('Rango'), ylabel('Intensidad'), xlim([0 256])
        
    % ecualizacion de la imagen    
    img_eq = img_gray;
    range = r_max-r_min;
    for i=1:height
        for j=1:width
            r = double(img_gray(i,j));            
            s = 255 * ( ( r - r_min )  / range );
            img_eq(i,j) = round(s);            
            
        end    
    end
    
end


%%
function hist = get_histogram(img)
    [height, width, depth] = size(img);
    
    L = 256;
    hist = zeros(L,1);    
    
    % obtener el histograma de la imgn
    for i=1:height
        for j=1:width
            value = img(i,j)+1;
            hist(value) = hist(value) + 1;
        end    
    end    
end


function [cum_distribution, r_min, r_max] = cumulative_distribution(hist_norm, p_min, p_max)
    
    L = 256;
    cum_distribution = zeros(L,1); 
    
    %obtener la distribucion acumulada, junto con sus valores min y max
    cum_distribution(1) = hist_norm(1);
    r_min = 0;
    r_max = 0;    
    for i=2:L
        cum_distribution(i) = cum_distribution(i-1)+hist_norm(i);
        if(r_min == 0 && cum_distribution(i) >= p_min)
            r_min = i-1;
        end
        
        if(r_max == 0 && cum_distribution(i) >= p_max)
            r_max = i-1;
        end
    end
     
    %estirar los niveles de intensidad para cubrir el rango 
    hist_eq = zeros(L,1);    
    temp = r_max-r_min;
    for i=1:L
        if(cum_distribution(i) > 0)
            s = round(255*( (i-r_min) / temp));
            if( s >= 0 && s <= L)
                hist_eq(s+1) = hist_norm(i);
            end
        end
    end    
    
end