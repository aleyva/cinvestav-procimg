% Algoritmo para deteccion de contorno en N 4 :
function img_edge = get_edge(img_edge, height, width)
    
    % 1. Definir código de posiciones en N 4 : →= 0, ↑= 1, ←= 2, ↓= 3.
    % 2. Buscar el primer píxel (p0) de la región conexa a partir de la
    % esquina superior izquierda de la img_tempn.    
    pi_i = 0;
    for i = 2:height-1
        for j = 2:width-1
            if img_edge(i, j) ~= 0      
                % 3. Inicializar variables
                pi_i = i;
                pi_j = j;
                break;
            end
        end    
        if pi_i ~= 0
            break;
        end
    end
    
    % 
    %[xmin ymin width height] that specifies the
    i_min = pi_i;
    j_min = pi_j;
    i_max = pi_i;
    j_max = pi_j;
    
    
    d = 0;
    do = true;                
    while do
        %N4 : →= 0, ↑= 1, ←= 2, ↓= 3                    
        % 4. Determinar en N 4 (p i ) el inicio de la búsqueda en la posición del
        % vecino q = (d + 3) mód 4.
        q = mod(d + 3, 4); 
        p_encontrado = 0; 
        img_edge(pi_i, pi_j) = -1;

        if (pi_j < j_min)
            j_min = pi_j;
        end
        if (pi_j > j_max)
            j_max = pi_j;
        end
        if (pi_i > i_max)
            i_max = pi_i;
        end
        
        
        % 5. A partir de q buscar en sentido antihorario el primer píxel con
        % valor 1, el cual es el nuevo elemento p i del contorno y actualizar
        % el valor de d con el código de su posición.                    
        for k = 0:3                     
            switch mod(k+q,4)
                case 0 % derecha
                    if img_edge(pi_i, pi_j + 1) ~= 0                                        
                        p_encontrado = 1; 
                        pi_j = pi_j + 1;
                        d = 0;
                        break;
                    end
                case 1 % arriba
                    if img_edge(pi_i - 1, pi_j) ~= 0                                                  
                        p_encontrado = 1;
                        pi_i = pi_i - 1;
                        d = 1;
                        break;
                    end
                case 2  % izquierda
                    if img_edge(pi_i, pi_j - 1) ~= 0                    
                        p_encontrado = 1;
                        pi_j = pi_j - 1;
                        d = 2;       
                        break;
                    end
                case 3 % abajo
                    if img_edge(pi_i + 1, pi_j) ~= 0                    
                        p_encontrado = 1;
                        pi_i = pi_i + 1;
                        d = 3;
                        break;
                    end
            end
        end

        % Si el píxel actual p i es igual al punto inicial del borde p 0 entonces
        % terminar el algoritmo; en caso contrario, regresar al Paso 4. 
        if i == pi_i && j == pi_j || p_encontrado == 0 
            do = false;
        end
    end
                
    img_edge(img_edge ~= -1) = 0;
    img_edge(img_edge == -1) = 1;
    
    rect = [j_min i_min j_max-j_min+1 i_max-i_min+1 ];    
    img_edge = imcomplement(imcrop(img_edge,rect));
    
    
end
            

