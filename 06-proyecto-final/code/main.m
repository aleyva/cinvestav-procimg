%%
close all;
clear all;

path = './';
img_name = 'img1.dcm';

% files = dir(fullfile(path,'*.tif')); 
file = fullfile(path,img_name); 

% read image
dicom = dicomread(file);

%% Segmentation by intensity
% define limits to clean not interesting information
i_muscle = filter_dicom(dicom,[950,1120]); 
i_bone = filter_dicom(dicom,[1100,2300]); 
i_all = filter_dicom(dicom,[950,2300]); 

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {dicom, i_muscle, i_bone, i_all};
    titles = {'DICOM', 'muscle','bone', 'muscle and bone'};
    window_bound = [.3 .45 .5 .85];
    show_plots([2 2],images,window_bound,titles);
end

%% find the interest region

% define morfological structured element
se1 = strel('disk',1);
se2 = strel('disk',2);
se3 = strel('disk',3);
se4 = strel('disk',4);
se5 = strel('disk',5);
se6 = strel('disk',6);

i_morf1 = i_all;
%quitar lineas de ruido
%i_morf1 = imopen(i_morf1, strel('line',8,90));
i_morf1 = imopen(i_morf1, se1);
%rellenar forma
i_morf2 = imdilate(i_morf1,se5);
i_morf2 = imfill(i_morf2,'holes');
i_morf2 = imerode(i_morf2, se5);
%quitar ruido restante
i_morf3 = imopen(i_morf2, se6);

%obtener mascara
muscle_mask = i_morf3;
muscle_mask(muscle_mask>0) = 1;
i_muscle_mask = i_muscle .* muscle_mask;

if(0)    
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle, i_morf1, i_morf2, i_morf3, muscle_mask, i_muscle_mask};
    titles = {'muscle', 'remove thin structures','dilate-fill-erode', ...
        'remove thin structures', 'mask', 'final'};
    window_bound = [.2 .65 .1 .85];
    show_plots([2 3],images,window_bound,titles);        
end

%% calculate the crop region and crop the original image
% Get all the blob properties.  
% Can only pass in originalImage in version R2008a and later.
mucle_prop = regionprops(muscle_mask, 'BoundingBox');
crop_rect = mucle_prop.BoundingBox + [-5,-5,10,10];

i_muscle_crop = imcrop(i_muscle_mask, crop_rect);
i_bone_crop = imcrop(i_bone, crop_rect);
i_all_crop = imcrop(i_all, crop_rect);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_crop, i_bone_crop, i_all_crop};
    titles = {'muscle','bone', 'muscle and bone'};
    window_bound = [.1 .85 .35 .5];
    show_plots([1 3],images,window_bound,titles);
end

%% crop the muscle near the spine 
[height, width] = size(i_muscle_crop);
crop_rect2 = [width/6,height/2,width/1.5,height/2];

i_muscle_crop2 = imcrop(i_muscle_crop, crop_rect2);
i_bone_crop2 = imcrop(i_bone_crop, crop_rect2);
i_all_crop2 = imcrop(i_all_crop, crop_rect2);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_crop2, i_bone_crop2, i_all_crop2};
    titles = {'muscle','bone', 'muscle and bone'};
    window_bound = [.1 .85 .35 .5];
    show_plots([1 3],images,window_bound,titles);
end

%% substract the bone of the original img
i_bone_bw = i_bone_crop2;
i_bone_bw(i_bone_bw>0) = 1;

%quitar lineas de ruido
i_bone_mf = imopen(i_bone_bw, se1);
i_bone_mf = imfill(i_bone_mf,'holes');

i_muscle_filt = i_muscle_crop2 .* uint8(~i_bone_mf);


%% segmenta el musculo

%morfologico
img_morf1 = i_muscle_filt;    

img_morf1 = imopen(img_morf1, se3);
img_morf2 = imfill(img_morf1,'holes');

img_morf2(img_morf2~=0)=1;
img_morf2 = imerode(img_morf2, se1);

% labeled the bit image
img_lab = bwlabel(img_morf2);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {i_muscle_filt, img_morf1, img_morf2, img_lab};
    titles = {'i_muscle_filt', 'img_morf1','img_morf2', 'img_lab and bone'};
    window_bound = [.12 .77 .1 .8]; % width, height
    show_plots([2 2],images,window_bound,titles);    
end

% Get the centroids and plot
prop2 = regionprops(img_lab, 'Area','Centroid');
centroids = cell2mat(struct2cell(prop2)');
%get the center point 
centroids(find(centroids(:,1) < 150),:) = [];

if(0)    
    rect_cen   = [90 40;90 70; 140 40; 140 70];   % rectangle position in the middle of the image
    color = {'green','green','green','green'};
    img_with_mark = insertMarker(i_muscle_filt,pos,'x','color',color,'size',5);
    figure, imshow(img_with_mark);
end

%left line, right line, up line, down line
rect_line   = [90 140 40 70];   % rectangle position in the middle of the image
seeds = zeros(3,2);
for i=1:length(centroids(:,1))
    if(centroids(i,2) > rect_line(1) & centroids(i,2) < rect_line(2))
        if(centroids(i,3) > rect_line(3) & centroids(i,3) < rect_line(4))
            seeds(1,:) = centroids(i,2:3);
            break;
        end
    end
end
%get the side points
centroids(find(centroids(:,1) < 1000),:) = [];
[~,idx] = sort(centroids(:,3),'descend'); % sort just the third
seeds(2:3,:) = centroids(idx(1:2),2:3);   % sort the whole matrix using the sort indices
seeds = round(seeds);

pos   = [seeds(1,1) seeds(1,2);seeds(2,1) seeds(2,2);seeds(3,1) seeds(3,2)];
color = {'green','green','green'};
img_with_mark = insertMarker(i_muscle_filt,pos,'x','color',color,'size',5);

im_temp = imbinarize(i_muscle_filt);
im_temp1 = imopen(im_temp, se1);
% figure, imshow(im_temp1);
im_temp1 = imdilate(im_temp1,se2);
im_temp1 = imfill(im_temp1,'holes');
im_temp1 = imerode(im_temp1, se2);
% figure, imshow(im_temp1);



mask1 = region_growing(i_muscle_filt, seeds(1,2), seeds(1,1), 65);
mask2 = region_growing(im_temp1, seeds(2,2), seeds(2,1), 1);
mask3 = region_growing(im_temp1, seeds(3,2), seeds(3,1), 1);

mask1 = imfill(mask1,'holes');
mask2 = imfill(mask2,'holes');
mask3 = imfill(mask3,'holes');

% the union of 
img_seg = (i_muscle_filt .* uint8(~mask1 & (mask2 | mask3)));

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {img_with_mark, mask1, mask2, mask3, uint8(~mask1 & (mask2 | mask3)), img_seg};
    titles = {'img_with_mark', 'center','mask2','mask3', 'mask_final' 'img_seg'};
    window_bound = [.12 .77 .2 .6]; % width, height
    show_plots([2 3],images,window_bound,titles);    
end


%% image equalization

[height, width] = size(img_seg);
percent = sum(img_seg(:)~=0) / (height*width);
img_eq = image_equalization(img_seg,percent,0.96);
img_imadjust = imadjust(img_seg);
img_histeq = histeq(img_seg); 
img_adapthisteq = adapthisteq(img_seg);
if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {img_seg, img_eq, img_imadjust, img_histeq, img_adapthisteq};
    titles = {'original', 'img_eq', 'img_imadjust','img_histeq', 'img_adapthisteq'};
    window_bound = [.05 .9 .3 .7]; % width, height
    show_plots([2 3],images,window_bound,titles);    
end

% estas dos son las buenas
% img_eq = image_equalization(img_seg,percent,0.96);
% img_imadjust = imadjust(img_seg);

% get the filter D
D = filter_D(img_seg);

% d0 lo dejo en 10, 20, 30 o 40?
% butterworth(img,d0,n)     
img_blur = f_butterworth(img_seg,D,30,1);
%im_final_b = hf_butterworth(img,D,d0,n,alpha,beta);    
img_but = hf_butterworth(img_blur,D,500,1,1.5,.5); 
%im_final_g = hf_gaussian(img,D,d0,n,alpha,beta);
img_gauss = hf_gaussian(img_blur,D,500,1,.7,.5);

img_but_s = imsharpen(img_but);
img_gauss_s = imsharpen(img_gauss);

% showing results
if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {img_seg, img_but, img_but_s, img_seg, img_gauss, img_gauss_s};
    titles = {'original', 'img_but','img_but_s', 'img', 'img_gauss', 'img_gauss_s'};
    window_bound = [.05 .9 .3 .7]; % width, height
    show_plots([2 3],images,window_bound,titles);    
end

if(0)
    % img = img_eq;
    % img = img_imadjust;
    img = img_seg;
    D = filter_D(img); 
    D = filter_D(img); 
    D = filter_D(img); 
    % d0 lo dejo en 10, 20 o 30?
    d0 = 30;
    % gaussian(img,d0)
    im_nr_gaussian = f_gaussian(img,D,d0);
    % butterworth(img,d0,n)     
    im_nr_butter = f_butterworth(img,D,d0,1);
    % figure, imshow(im_nr_butter,[]);
    change = img-im_nr_butter;

    img_bw = imbinarize(img-change,.29);

    if(0)
        %function show_plots(plot, imgs, window_bound, titles)   
        images = {img, change, img-change, img_bw};
        titles = {'original', 'change','img-change', 'img_bw'};
        window_bound = [.12 .77 .1 .8]; % width, height
        show_plots([2 2],images,window_bound,titles);    
    end
end

%% img binarizing 

img_eq_bw = imbinarize(img_eq,.73);
img_imadjust_bw = imbinarize(img_imadjust,.65);
img_seg_bw = imbinarize(img_seg,.47);

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {img_seg, img_imadjust, img_seg, img_eq_bw, img_imadjust_bw, img_seg_bw};
    titles = {'original', 'img_imadjust','img_seg','img_eq_bw','img_imadjust_bw', 'img_seg_bw'};
    window_bound = [.05 .9 .3 .7]; % width, height
    show_plots([2 3],images,window_bound,titles);    
end

%% get the area

% bw_morf = imdilate(img_eq_bw,se4);
% bw_morf = imfill(bw_morf,'holes');
% bw_morf = imerode(bw_morf, se5);

% area_bw = img_imadjust_bw + bw_morf;
% area_bw(area_bw==2) = 1;

left_muscle_bw = img_imadjust_bw .* mask3;
right_muscle_bw = img_imadjust_bw .* mask2;

bw_morf_l = imdilate(left_muscle_bw,se3);
bw_morf_l = imfill(bw_morf_l,'holes');
bw_morf_l = imerode(bw_morf_l, se3);
area_bw_l = left_muscle_bw | bw_morf_l;

bw_morf_r = imdilate(right_muscle_bw,se3);
bw_morf_r = imfill(bw_morf_r,'holes');
bw_morf_r = imerode(bw_morf_r, se3);
area_bw_r = right_muscle_bw | bw_morf_r;

if(0)
    %function show_plots(plot, imgs, window_bound, titles)   
    images = {left_muscle_bw, right_muscle_bw, area_bw_l, area_bw_r};
    titles = {'left_muscle_bw', 'right_muscle_bw','left_muscle_area','right_muscle_bw'};
    window_bound = [.12 .77 .1 .8]; % width, height
    show_plots([2 2],images,window_bound,titles);    
end

pixels_muscle_l = sum(left_muscle_bw(:));
pixels_muscle_r = sum(right_muscle_bw(:));
pixels_area_l = sum(area_bw_l(:));
pixels_area_r = sum(area_bw_r(:));

muscle_percent_l = pixels_muscle_l / pixels_area_l;
muscle_percent_r = pixels_muscle_r / pixels_area_r;

%% showing results

% labeled_img = bwlabel(image, 8); 
% prop3 = regionprops((mask2 | mask3), 'BoundingBox');
% numberOfBlobs = size(blobMeasurements, 1);

str = "Left muscle area %d, left muscle without grease %d \n";
str = str + "Right muscle area %d, right muscle without grease %d \n";
str = str + "grease filtration in left muscle %4.2f \n";
str = str + "grease filtration in right muscle %4.2f \n";
str = sprintf(str, pixels_area_l, pixels_muscle_l, pixels_area_r, ...
    pixels_muscle_r, (1 - muscle_percent_l) *100,  (1 - muscle_percent_r) *100);
 
figure, imshow(i_muscle_crop2);
title(sprintf(str, pixels_area_l, pixels_muscle_l, ...
    pixels_area_r, pixels_muscle_r, (1 - muscle_percent_l) *100,  (1 - muscle_percent_r) *100)); 
hold on;
boundaries = bwboundaries(~mask1 & (mask3 | mask2));
for k = 1 : size(boundaries, 1)
	thisBoundary = boundaries{k};
	plot(thisBoundary(:,2), thisBoundary(:,1), 'g', 'LineWidth', 2);
end
hold off;

%%
if(0)
    imwrite(i_all_crop2,'i_all_crop2.jpeg')
    imwrite(i_muscle_crop2,'i_muscle_crop2.jpeg')    
end


%% filter a dicom image with bound threshold
function img = filter_dicom(dicom, bound)    
    dicom(dicom<bound(1)) = 0;
    dicom(dicom>bound(2)) = 0;
    dicom = dicom - bound(1) - 1; 
    img = uint8(255*mat2gray(dicom));
end
