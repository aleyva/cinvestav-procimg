


% Para cada región contextual:
% 1) Calcular el histograma de la región.
% 2) ‘Recortar’ el histograma en un umbral predefino (clip).
% 3) Distribuir uniformemente los valores recortados sobre todo el
% histograma para mantener el número total de píxeles.
% 4) Generar la función T (k) dada en (10) a partir del histograma
% modificado.
% 2. Mapear los píxeles de cada región contextual usando las funcio-
% nes T (k) generadas aplicando interpolación bilineal dada en (11).

clear all;
close all;

%ruta de las imagenes
path = './img';
% patron para elegir archivos
files = dir(fullfile(path,'*.tif')); 
num_img = 2; %imagenes del 1 al 6

for num_img = 1:numel(files)      
    img_path = fullfile(path,files(num_img).name);                
    %img_path = '/mnt/Home/Proyectos/Maestria/12 Analisis de Imagenes/02 Tareas-Proyecto/02-tarea/images02/einstein.tif';
    tiff_obj = Tiff(img_path,'r');
    
    % leer la imagen
    % [Y,Cb,Cr] = read(tiff_obj);
    % img = read(tiff_obj);
    img = tiff_obj.read();    
    
    if size(img,3) == 3 % image is in color
        [H, S, I] = rgb_to_hsi(img);          
        img_gray = round(I * 255);
    else
        img_gray = img;
    end
    
    % histograma
    [height, width, depth] = size(img_gray);    
    hist = get_histogram(img_gray);    
    hist_norm = hist / (height * width);
    
    % equalizacion normal
    img_norm_eq = image_equalization(img_gray,0.4,0.96);
    
    % equalizacion adaptativa (CLAHE)
    context_reg = [4 4];    
    clip_level_img = [0.08 0.023 0.028 0.04 0.34 0.02];
    clip_level = clip_level_img(num_img);    
    print_hist = 1;
    img_ad_eq = adaptive_equalization(img_gray, clip_level, context_reg);
        
    
    if size(img,3) == 3 % image is in color
        img_ad_eq = img_ad_eq / 255;
        img_ad_eq = hsi_to_rgb(H, S, img_ad_eq);           
    end
        
    
    % imprimir resultados
    figure, 
    subplot(4,4,[1,2,5,6]), imshow(img)    
    title(sprintf('Imagen original, %s', files(num_img).name));    
    subplot(4,4,[3,4,7,8]), bar(hist_norm)    
    title('Histograma'), xlabel('Rango'), ylabel('Intensidad'), xlim([0 256]);    
    subplot(4,4,[9,10,13,14]), imshow(img_norm_eq, [0 255])    
    title('Imagen con histograma equalizado');
    subplot(4,4,[11,12,15,16]), imshow(img_ad_eq)     
    title(sprintf('CLAHE, cliplevel %1.2f, regiones [%d %d]', clip_level, context_reg(1), context_reg(2)));                
    

    %saveas(gcf,sprintf('Resultados_%d.png', num_img));
    
    close(tiff_obj);
end



