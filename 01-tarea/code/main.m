

clear;

%ruta de las imagenes
path = './img';
% patron para elegir archivos
files = dir(fullfile(path,'*.png')); 


for file = 1:numel(files)    
    image_path = fullfile(path,files(file).name);                
    % image_path = '/home/spidey/Git/cinvestav-procimg/01-tarea/img_source/zoo.png';        
    
    % leer la imagen
    [image, height, width] = read_image(image_path);
    
    % etiquetar la imagen
    [image_t, n_classes] = get_connected(image, height, width);
    
    image_rgb = label2rgb(image_t);
    figure, imshow(image_rgb);
    
    for i = 1:n_classes
        img_temp = image_t;
        img_temp(img_temp ~= i) = 0;
        img_edge = get_edge(img_temp, height, width);
        % figure, imshow(img_edge);        
        % filename = sprintf('/home/spidey/Git/cinvestav-procimg/01-tarea/img_results/zoo_%02d.png', i);
        % imwrite(img_edge, filename);
    end
    figure, imshow(img_edge); % mostrar solo el último elemento de la imagen
end