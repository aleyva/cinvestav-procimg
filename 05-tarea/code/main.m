% 1. Implementar una rutina que calcule los siete momentos invariantes (momentos de Hu).
% 2. Implementar una rutina que calcule los descriptores de Fourier dado un número de
% coeficiente complejos de entrada.
% 3. Usar ambas rutinas para calcular rasgos de forma de las 10 clases de objetos que se
% proveen en el material de apoyo. De cada clase hay 20 imágenes con una forma binaria
% que representa la segmentación de un objeto.
% 4. Para cada técnica de extracción de rasgos, clasificar los patrones resultantes utilizando
% el algoritmo de los k-vecinos más cercanos. La clasificación se realizará mediante el
% método de validación cruzada dejando uno fuera, esto es, una sola muestra se clasifica
% usando al resto de las muestras como datos de entrenamiento. Este proceso se repite
% hasta que todas las muestras fueron clasificadas (se recomienda que investigues más al
% respecto).
% 5. Explorar el efecto de variar el número de vecinos para k=1,3,7,9. También, explorar el
% efecto de usar diferente número de descriptores de Fourier, por ejemplo, 5, 10, 15, ...
% 6. Reportar la tasa de reconocimiento por clase, así como la exactitud global.
% 7. Se permiten usar funciones y/o bibliotecas para el cálculo de la FFT y del algoritmo de
% los k-vecinos más cercanos.

% parameters
path = './datos';

%% get a dataset struct with files info and their classes
dataset = dir(fullfile(path,'*/*.gif')); 
for i = 1:length(dataset)
    [~, dataset(i).class] = fileparts(dataset(i).folder); 
    dataset(i).file = fullfile(dataset(i).folder,dataset(i).name);    
end
% convert dataset struct to cell
dataset = struct2cell(dataset)';
% filter the dataset and convert to array
% - classes: eigth column is
% - filename: seventh column is
files = dataset(:,8);
classes = dataset(:,7)';
classes_u = unique(classes);


%% get the descriptors accuracy and show results HU
k = 2;
get the hu shape descriptors
hu_sf = hu_shape_descriptors(files);
[h_class_predicted, h_class_acc, h_total_acc] = leave_one_out_knn(hu_sf, classes, k);


fprintf('\n\nHU Accuracy by class\n');
for i=1:length(classes_u)
    fprintf('   %s: %4.2f \n',char(classes_u(i)), h_class_acc(i));
end
fprintf('HU Global accuracy: %4.2f', h_total_acc);

%% get the descriptors accuracy and show results fourier
k = 3;
p_fourier = 25;
% get the fourier shape descriptors
fourier_sf = fourier_shape_descriptors(files, p_fourier);
[f_class_predicted, f_class_acc, f_total_acc] = leave_one_out_knn(fourier_sf, classes, k);

fprintf('\n\nFourier Accuracy by class, k=%d, points=%d \n',k,p_fourier);
for i=1:length(classes_u)
    fprintf('   %s: %4.2f \n',char(classes_u(i)), f_class_acc(i));    
end
fprintf('Fourier Global accuracy: %4.2f \n', f_total_acc);



