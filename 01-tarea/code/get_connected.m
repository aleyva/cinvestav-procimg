function [img_t, total_labels] = get_connected(img, height, width)

    n = 5000;
    eq_rel = zeros(n, n); %creacion arbitraria de matriz 1000x1000
   
    % Algoritmo de labeldo de imgnes conexas para 4-adyacencia        
    label = 0;    
    lp = 0;
    lq = 0;
    for i = 2:height-1
        for j = 2:width-1
            if img(i, j) == 1
                lp = img(i - 1, j);
                lq = img(i, j - 1);
                if lp == 0 && lq == 0                                
                    label = label + 1;                    
                    img(i, j) = label;
                elseif (lp ~= lq) && (lp ~= 0) && (lq ~= 0)                                        
                    eq_rel(lp, lq) = 1; %Registrar equivalencia (lp, lq)
                    img(i, j) = lp;                    
                elseif lq ~= 0
                    img(i, j) = lq;
                elseif lp ~= 0
                    img(i, j) = lp;                
                end
            end
            %img(i, j) = lx;
        end
    end
    
    %imshow(img, [0 label]);

    % Hacer "eq_rel" reflexiva, simétrica y transitiva
    bm = bitor( bitor(eq_rel, transpose(eq_rel)),  eye(n));
    
    % Algoritmo para encontrar relaciones de equivalencias (warshall)    
    max_label = label;
    for k = 1:max_label
        for i = 1:max_label
            for j = 1:max_label % for j = i:max_label
                if( bm(i, k) == 1 && bm(k, j) == 1)
                    bm(i, j) = 1;
                end
                %bm(i, j) = bitor(bm(i, j), bitand(bm(i, k), bm(k, j)) );
            end
        end
    end
    
    % Algoritmo para clases de equivalencia
    classes = unique(bm(1:max_label,1:max_label),'rows');
    img_t = img;
    new_label = max_label +1;
    for i = 1:size(classes,1)
        class = find(classes(i,:)==1); % por fila
        for k = 1:length(class)
            img_t(img_t==class(k)) = new_label;
        end
        new_label = new_label +1;
    end
    
    % hacer que la imgn tenga etiquetas de clases iniciando 
    % en [ 1, 2, ... , n]
    img_t(img_t == 0) = max_label;
    img_t = img_t - max_label;
    
    %imshow(img_t, [0 size(classes,1)]); %mostrar la imgn
    total_labels = new_label - max_label - 1;    
    
end