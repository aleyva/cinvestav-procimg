% Gabor filter in frequency domain
    % height, width:    filter size (image size)
    % n_angle_filters:  number of filters in each radial frequency
    % bandwidth:        controls the overall size of the Gabor envelope
    % n_angle_filters:  number of total angle filters (usually set to 8)
function [filters_g, gabor_f_img, sum_gfi] = gabor_filters(img, bandwidth, n_angle_filters)
    
    b = bandwidth;
    
    [height, width] = size(img);
    
    % DFT of logged image
    im_f = fft2(img);
    im_f = fftshift(im_f);

    % image center
    c_m = height/2;
    c_n = width/2;

    n_radial_freq = ceil(log2(width/4));
    % u0 = zeros(1, n_radial_freq);

    % all the gabors filtesrs
    filters_g = cell( n_radial_freq+1 , n_angle_filters );

    % image of all gabor filters together
    gabor_f_img = zeros(height,width);
    sum_gfi = zeros(height,width);

    % loop for radial frequency
    for u = 0:n_radial_freq       
        u_k = power(2,u)*sqrt(2);    % the radial frequency
        % u0(u+1) = u_k;

        sigma_b = 1/(2*u_k) * sqrt(log(2)/2) * ( (2^b)+1 / (2^b)-1 );
        sigma = 1 / (2*pi*sigma_b); 
        sigma_pow2 = sigma^2;

        for step=1:n_angle_filters
            teta = step*(180/n_angle_filters);
            g_filter = zeros(height,width);

            % cycles to calculate each gabor filter
            for m = 1:height
                for n = 1:width
                    x_p = (m-c_m)*cosd(teta) + (n-c_n)*sind(teta);
                    y_p = -(m-c_m)*sind(teta) + (n-c_n)*cosd(teta);  
                    term = (y_p^2) / sigma_pow2;
                    g_filter(m,n) = exp(-0.5*( ((x_p-u_k)^2 / sigma_pow2) + term )) + ...
                                    exp(-0.5*( ((x_p+u_k)^2 / sigma_pow2) + term ));
                end
            end
            
            % Filter Applying DFT image
            im_nf = g_filter.*im_f;
            % Inverse DFT of filtered image
            im_nf = ifftshift(im_nf); 
            % add the gabor filter to the gabor bank filters
            filters_g{u+1,step} = abs(ifft2(im_nf));
            
            % result image, not important for the process
            gabor_f_img = gabor_f_img + g_filter;
            sum_gfi = sum_gfi + filters_g{u+1,step};
        end
    end
    
end