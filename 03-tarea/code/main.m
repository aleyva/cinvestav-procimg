clear all;
close all;

%ruta de las imagenes
path_noisy = './img/noisy';
path_original = './img/original';
% patron para elegir archivos
files_noisy = dir(fullfile(path_noisy,'*.tif')); 
files_original = dir(fullfile(path_original,'*.tif'));

num_img = 6; %imagenes del 1 al 6

% for num_img = 1:numel(files_noisy)      
    img_path = fullfile(path_noisy,files_noisy(num_img).name);                
    img_path_o = fullfile(path_original,files_original(num_img).name);                    
    
    % reading images
    tiff_obj = Tiff(img_path,'r');    
    img = tiff_obj.read(); 
    close(tiff_obj);       
    tiff_obj = Tiff(img_path_o,'r');    
    img_o = tiff_obj.read(); 
    close(tiff_obj);
    
    % get the filter D
    D = filter_D(img);    
    
    d0 = 100;
    n = 1;
    % gaussian(img,d0)
    im_nr_gaussian = f_gaussian(img,D,d0);
    % butterworth(img,d0,n)     
    im_nr_butter = f_butterworth(img,D,d0,n);    
    
    d0 = 200;    
    alpha = 0.6;
    beta = 0.3;    
    im_final_b = hf_butterworth(im_nr_butter,D,d0,n,alpha,beta);    
    alpha = 0.6;
    beta = 1;
    im_final_g = hf_gaussian(im_nr_butter,D,d0,n,alpha,beta);
    alpha = 1;
    beta = .7;    
    im_final_e = hf_exponent(im_nr_butter,D,d0,n,alpha,beta);        
    
    % showing results
    monitor_dimension = get(0, 'MonitorPositions');
    figure_position = [monitor_dimension(3)*.1,monitor_dimension(4)*.1,monitor_dimension(3)*.8,monitor_dimension(4)*.8];
    figure('position',figure_position);    
    subaxis(2,3,1,'SH',0,'SV',0,'M',0,'PT',.02);
    imshow(img_o); title(sprintf('Original (NOT noisy) %s',files_original(num_img).name));        
    subaxis(2); imshow(img); 
    title(sprintf('Original (noisy) %s',files_noisy(num_img).name));
    subaxis(3); imshow(im_nr_butter); 
    title(sprintf('noise reduction filter'));                
    subaxis(4); imshow(im_final_b); 
    title(sprintf('Homomorphic f. butterworth'));        
    subaxis(5); imshow(im_final_g); 
    title(sprintf('Homomorphic f. gaussian'));        
    subaxis(6); imshow(im_final_e); 
    title(sprintf('Homomorphic f. exponent'));        
    
% end 
    