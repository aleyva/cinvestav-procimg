function img_eq = image_equalization(img_gray, p_min, p_max)
    
    [height, width, depth] = size(img_gray);

    % obtener el histograma de la imagen
    hist = get_histogram(img_gray);    
    hist_norm = hist / (height * width);
    % figure, bar(hist_norm);
    % title('Histograma normalizado'), xlabel('Rango'), ylabel('Intensidad'), xlim([0 256])
    
    % obtener la distribucion acumulativa, junto con los valores min y max
    [cum_dist, r_min, r_max] = cumulative_distribution(hist_norm, p_min, p_max);       
    % figure, bar(cum_dist);
    % title('Distribucion acumulativa'), xlabel('Rango'), ylabel('Intensidad'), xlim([0 256])
        
    % ecualizacion de la imagen    
    img_eq = img_gray;
    range = r_max-r_min;
    for i=1:height
        for j=1:width
            r = double(img_gray(i,j));            
            s = 255 * ( ( r - r_min )  / range );
            img_eq(i,j) = round(s);            
            
        end    
    end
    
end
