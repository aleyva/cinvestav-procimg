function [class_predicted, class_acc, total_acc] = leave_one_out_knn(values, classes, k)

    % get the class prediction of each file with knn
    n_files = length(values);  
    class_predicted = cell(size(classes));  
    for i = 1:n_files
        % leave-one-out part 
        leaveoo_values = values(1:end ~= i,:);
        leaveoo_classes = classes(1:end ~= i);
        % getting the k-nearest neighbor
        mdl = fitcknn(leaveoo_values,leaveoo_classes,'NSMethod','exhaustive','NumNeighbors',k,'Distance','correlation');
        % get the vector of predicted class labels for the predictor data in values
        class_predicted(i) = predict(mdl,values(i,:));
    end
        
    % calculate the accuracy of each class
    classes_u = unique(classes);
    num_classes = length(classes_u);
    class_acc = zeros(1,num_classes);
    for i = 1:num_classes        
        label = classes_u(i);
        filter_by_class = class_predicted(strcmp(classes, label));
        n_correct = sum(strcmp(filter_by_class, label) );                
        class_acc(i) = n_correct/length(filter_by_class);
    end
    
    % calculate the total accuracy
    correct = strcmp(class_predicted,classes);    
    total_acc = sum(correct)/n_files;
    
end