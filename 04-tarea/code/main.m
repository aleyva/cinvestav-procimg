% 1. Deberá contener rutinas para calcular los cuatro pasos básicos del método:
%     1) descomposición multicanal con un banco de filtros de Gabor, 
%     2) filtrado suavizante con banco de filtros Gaussianos, 
%     3) selección de canales relevantes, y 
%     4) agrupamiento con algoritmo k-means.
% 2. Los parámetros libres que puede ajustarse son
%     el paso de ángulo (θ), 
%     el ancho de banda (b), 
%     la constante de factor de suavizado (κ), 
%     y el número de grupos k para el algoritmo k-means.


clear all;
close all;

%images path
path = './img';
% files filter
files = dir(fullfile(path,'*.tif')); 

num_img = 6; %imagenes del 1 al 7

% for num_img = 1:numel(files)-1      
    img_path = fullfile(path,files(num_img).name);                
        
    % reading images
    tiff_obj = Tiff(img_path,'r');    
    img = tiff_obj.read(); 
    close(tiff_obj);       
    
    [bandwidth,angle,kappa,k] = get_arguments(num_img, numel(files));    
    
    % 1. Descomposicion multicanal
    [gabor_f, gabor_f_img, sum_gfi] = gabor_filters(img, bandwidth, angle);        
         
    % 2. Filtrado suavisante
    gauss_f = gaussian_filters(gabor_f, kappa);    
        
    % 3. Seleccion de canales
    selection = channels(gauss_f);
    
    % 4. k-means
    output = k_means_f(selection, k);
    
    % showing results
    monitor_dimension = get(0, 'MonitorPositions');
    figure_position = [monitor_dimension(3)*.1,monitor_dimension(4)*.1,monitor_dimension(3)*.8,monitor_dimension(4)*.8];
    figure('position',figure_position);    
    subaxis(2,3,1,'SH',0,'SV',0,'M',0,'PT',.02);
    imshow(img); title(sprintf('Original %s',files(num_img).name));        
    subaxis(2); imshow(gabor_f_img,[]); 
    title(sprintf('Gabor filters, bandwidth=%3.2f, angle=%3.2f',bandwidth, angle));
    subaxis(3); imshow(sum_gfi,[]); 
    title(sprintf('Sum of all gabor filters applied'));                
    subaxis(4); imshow(sum(gauss_f,3),[]); 
    title(sprintf('Sum with with gaussian blur, kappa=%3.2f',kappa));        
    subaxis(5); imshow(sum(selection,3),[]); 
    title(sprintf('Sum of selected channels'));        
    subaxis(6); imshow(output, []); 
    title(sprintf('Segmentation result, K=%d',k));        
    
    if(1)         
        % show the gabor filters applying to image
        imshow_filters_cell(gabor_f');
        % show the gaussian filters applying to image
        [h, w] = size(gabor_f);
        imshow_filters(gauss_f, h, w);        
    end
    
% end 
    