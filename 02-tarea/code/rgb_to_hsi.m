function [H, S, I] = rgb_to_hsi(image)
    % Normalizar en rango [0 1] 
    img=double(image)/255;

    R=img(:,:,1);
    G=img(:,:,2);
    B=img(:,:,3);

    %Hue
    numi=1/2*((R-G)+(R-B));
    denom=((R-G).^2+((R-B).*(G-B))).^0.5;

    %evitar excepcion al dividir entre 0
    H=acosd(numi./(denom+0.000001));

    %If B>G then H= 360-Theta
    H(B>G)=360-H(B>G);

    % Normalizar en rango [0 1]
    H=H/360;

    %Saturation
    S=1- (3./(sum(img,3)+0.000001)).*min(img,[],3);

    %Intensity
    I=sum(img,3)./3;

end