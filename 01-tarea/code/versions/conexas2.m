
clear;

%ruta de las imagenes
path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01';
% patron para elegir archivos
files = dir(fullfile(path,'*.png')); 


% for file = 3:numel(files)    
%     image_path = fullfile(path,files(file).name);            
    image_path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01/zoo.png';    
    image = mat2gray(imread(image_path));   %read image and convert to gray 
    image = padarray(image,[1 1],'both');   %add padding of 1 pixel
    [height, width, planes] = size(image);  % get the height and width of the image
    
    n = 1000;
    eq_rel = zeros(n, n); %creacion arbitraria de matriz 1000x1000
   
    % Algoritmo de tagdo de imagenes conexas para 4-adyacencia        
    tag = 0;    
    lp = 0;
    lq = 0;
    for i = 2:height-1
        for j = 2:width-1
            if image(i, j) == 1
                lp = image(i - 1, j);
                lq = image(i, j - 1);
                if lp == 0 && lq == 0                                
                    tag = tag + 1;                    
                    image(i, j) = tag;
                elseif (lp ~= lq) && (lp ~= 0) && (lq ~= 0)                                        
                    eq_rel(lp, lq) = 1; %Registrar equivalencia (lp, lq)
                    image(i, j) = lp;                    
                elseif lq ~= 0
                    image(i, j) = lq;
                elseif lp ~= 0
                    image(i, j) = lp;                
                end
            end
            %image(i, j) = lx;
        end
    end
    
    %imshow(image, [0 tag]);

    % Hacer "eq_rel" reflexiva, simétrica y transitiva
    bm = bitor( bitor(eq_rel, transpose(eq_rel)),  eye(n));
    
    % Algoritmo para encontrar relaciones de equivalencias (warshall)    
    max_tag = tag;
    for k = 1:max_tag
        for i = 1:max_tag
            for j = 1:max_tag % for j = i:max_tag
                if( bm(i, k) == 1 && bm(k, j) == 1)
                    bm(i, j) = 1;
                end
                %bm(i, j) = bitor(bm(i, j), bitand(bm(i, k), bm(k, j)) );
            end
        end
    end
    
    
    
    % vector of exploration
    
    
    
    %bm2 = bm(1:max_tag,1:max_tag);
    classes = unique(bm(1:max_tag,1:max_tag),'rows');
    image_t = image;
    max_tag = max_tag +1;
    for i = 1:size(classes,1)
        class = find(classes(i,:)==1); % por fila
        for k = 1:length(class)
            image_t(image_t==class(k)) = max_tag;
        end
        max_tag = max_tag +1;
    end

    imshow(image_t);
    
    
%     track = ones(1, max_tag);    
%     new_tag = 1;
%     for i = 1:max_tag
%         if (track(i) == 1)
%             class = find(bm(i,:)==1); % por fila
%             class_length = length(class);
%             track(i) = 0;
%             for j = 2:length(class)
%                 temp = find(bm(j,:)==1);                
%                 if(class_length ~= length(temp))
%                     class = unique([class temp]);
%                     class_length = length(class);
%                 end
%                 track(j) = 0;
%             end
%             
%             for k=1:length(class)
%                 image(image==class(k)) = new_tag;
%             end
%             new_tag = 2;
%         end
%     end
%     
%     imshow(bm);
% end



% 3. Escribir un programa que extraiga de manera individual los contornos de varias regiones
% conexas usando los programas de los puntos 1 y 2.
% 4. Redactar un reporte de la práctica con al menos los siguientes elementos: Introducción,
% Métodos, Resultados, Conclusiones y Referencias.
% 5. Enviar los códigos fuente en Matlab tareas.tamps.cinvestav@gmail.com
