function moments = hu_shape_descriptors(dataset)    
    
    % get hu moments for each file    
    num_files = length(dataset);
    moments = zeros(num_files,7);
    for i = 1:num_files
        moments(i,:) = hu_moments(char(dataset(i)));        
    end    
    
end