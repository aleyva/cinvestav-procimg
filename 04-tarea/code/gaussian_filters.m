function gauss_f = gaussian_filters(gabor_f, k)
        
    [f_h, f_w] = size(gabor_f);  
    [height,width] = size(gabor_f{1,1});  
    
    % shift to center
    [h,w] = meshgrid(0:height-1, 0:width-1);

    % shift to center
    h_p = h-(height/2);
    w_p = w-(width/2);
    h_p2 = h_p.*h_p;
    w_p2 = w_p.*w_p;
    
    % all the gaussian filtesrs    
    gauss_f = zeros(height,width,f_h*f_w);
    
    % image of all gaussr filters together    
    add_gauss_filters = zeros(height,width);
    
    % loop for radial frequencies
    for i = 1:f_h       
        % the radial frequency
        u_k = power(2,i)*sqrt(2);    
        % sigma
        sigma = k*width/u_k;      
        % kernel
        gauss_filter = exp(-(h_p2+w_p2) / (2*(sigma^2)) );
        
        % loop to each image in radial frequency
        for j = 1:f_w
            % DFT of logged image
            img = gabor_f{i,j}.*((-1).^(w+h));
            im_f = fft2(img);
            % im_f = fftshift(im_f);
            % Filter Applying DFT image
            im_nf = gauss_filter.*im_f;
            % Inverse DFT of filtered image
            % im_nf = ifftshift(im_nf); 
            % add the gabor filter to the gabor bank filters
            filter = abs(ifft2(im_nf));
            gauss_f(:,:,((i-1)*f_w)+j) = filter;
            
            add_gauss_filters = add_gauss_filters + filter;
        end        
    end
    % figure, imshow(add_gauss_filters,[]);
end