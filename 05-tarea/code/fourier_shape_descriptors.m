function fourier_descriptors = fourier_shape_descriptors(dataset, p_fourier)    
    
    % get fourier descriptors for each file    
    num_files = length(dataset);
    fourier_descriptors = zeros(num_files,2*p_fourier+1);
    for i = 1:num_files
        fourier_descriptors(i,:) = fourier_descriptor(char(dataset(i)),p_fourier);
    end
end


    