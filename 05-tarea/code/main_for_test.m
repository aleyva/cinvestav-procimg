

% parameters
path = './datos';

%% get a dataset struct with files info and their classes
dataset = dir(fullfile(path,'*/*.gif')); 
for i = 1:length(dataset)
    [~, dataset(i).class] = fileparts(dataset(i).folder); 
    dataset(i).file = fullfile(dataset(i).folder,dataset(i).name);    
end
% convert dataset struct to cell
dataset = struct2cell(dataset)';
% filter the dataset and convert to array
% - classes: eigth column is
% - filename: seventh column is
files = dataset(:,8);
classes = dataset(:,7)';
classes_u = unique(classes);


%% get the descriptors accuracy and show results HU
k = 4;


%% get the descriptors accuracy and show results fourier
k = [2, 3, 5, 10, 20, 35];
for m=1:length(k)
    % get the hu shape descriptors
    hu_sf = hu_shape_descriptors(files);
    [h_class_predicted, h_class_acc, h_total_acc] = leave_one_out_knn(hu_sf, classes, k(m));
    
    fprintf('\nHU Accuracy by class, k=%d\n',k(m));
    for i=1:length(classes_u)
        fprintf('   %s: %4.2f \n',char(classes_u(i)), h_class_acc(i));
    end
    fprintf('HU Global accuracy: %4.2f \n', h_total_acc);
    fprintf('------------------------------- \n\n');
end


%% get the descriptors accuracy and show results fourier
k = [2, 3, 5, 10, 20];
p_fourier = [10, 25, 50, 80];

for l=1:length(p_fourier)
    for m=1:length(k)
        % get the fourier shape descriptors
        fourier_sf = fourier_shape_descriptors(files, p_fourier(l));
        [f_class_predicted, f_class_acc, f_total_acc] = leave_one_out_knn(fourier_sf, classes, k(m));


        fprintf('\nFourier Accuracy by class, k=%d, points=%d \n',k(m),p_fourier(l));
        for i=1:length(classes_u)
            fprintf('   %s: %4.2f \n',char(classes_u(i)), f_class_acc(i));    
        end
        fprintf('Fourier Global accuracy: %4.2f \n', f_total_acc);
        fprintf('------------------------------- \n\n');
    end
end