function selection = channels(chann)
     
    % sum of all gauss images
    sum_gf = sum(chann,3);
    % sum of all gauss images, squeared
    sum_gf2 = sum(sum_gf(:).^2);

    % get the matrix dimensions
    [height, width, deep] = size(chann);
    
    % get the id of the channel with max R
    [r_val, id] = get_R(chann,sum_gf,sum_gf2, height*width, deep);
    % start the subset
    selection = chann(:,:,id);
    % drop the best channel from the initial set
    chann(:,:,id) = [];
    deep = deep-1;
    
    
    % 1. Inicializar un subconjunto de canales de textura, seleccionando
    % 2. Seleccionar el canal de textura que más aproxime a s(x, y) , es
    % decir, aquel con el mayor valor de R .        
    % 3. Remover el canal de textura seleccionado del conjunto original
    % y colocarlo en S .
    % 4. Seleccionar un canal de textura que sumado con todos los ca-
    % nales de textura en S (i.e., ŝ(x, y) ) aproxime mejor a s(x, y) .
    % 5. Repetir los pasos 3 y 4 hasta que R ≥ 0.95 .
        
    while r_val <= .98 && deep > 0
        saux1 = sum(selection,3);
        sum_p = saux1(:,:,ones(1,deep))+chann;
        % get the id of the channel with max R
        [r_val, id] = get_R(sum_p,sum_gf,sum_gf2, height*width, deep);
        % start the subset
        selection = cat(3,selection,chann(:,:,id));
        % drop the best channel from the initial set
        chann(:,:,id) = [];
        deep = deep-1;
    end    
end



function [r_val, id] = get_R(chann,sum_gf,sum_gf2, frame_size, deep)    
    
    % set the addition of all channels to tree dimensions
    sum_chann = sum_gf(:,:,ones(1,deep));
    
    % get the numerator of R
    difference = sum(reshape((chann-sum_chann).^2,frame_size,deep),1);
        
    % looking for the best texture channel
    [r_val, id] = max(1-(difference/sum_gf2));    
end