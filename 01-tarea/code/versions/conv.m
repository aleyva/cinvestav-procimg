
clear;

image_path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01/zoo.png';
image = mat2gray(imread(image_path));   
    
%add padding of 1
image = padarray(image,[1 1],'both');    
    
[height, width, planes] = size(image);
    
faler_h = [-1,0,1;-1,0,1;-1,0,1]; 
faler_v = [1,1,1;0,0,0;-1,-1,-1]; 
faler_d = [0,1,0;-1,0,1;0,-1,0]; 
faler_full = [-1,-1,-1;-1,8,-1;-1,-1,-1]; 

w = conv2(image, faler_full);
 
%imshow(image, [0 2]);
imshow(w);
