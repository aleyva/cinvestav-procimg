
clear;

image_path = '/home/spidey/Git/cinvestav-procimg/01-tarea/images01/zoo.png';
%read image and convert to gray 
image = mat2gray(imread(image_path));   

%add padding of 1 pixel
image = padarray(image,[1 1],'both');    
[height, width, planes] = size(image);

etiqueta = 2;
% Algoritmo para el seguimiento del contorno en N 4 :
% 1. Definir código de posiciones en N 4 : →= 0, ↑= 1, ←= 2, ↓= 3.
% 2. Buscar el primer píxel (p0) de la región conexa a partir de la
% esquina superior izquierda de la imagen.    
% for i = 2:height-1
%     for j = 2:width-1
%         if image(i, j) == 1
%             
            i = 2;
            j = 207;             
            % 3. Inicializar variables
            pi_i = i;
            pi_j = j;     
            d = 0;            
            
            do = true;                
            while do
                %N4 : →= 0, ↑= 1, ←= 2, ↓= 3                    
                % 4. Determinar en N 4 (p i ) el inicio de la búsqueda en la posición del
                % vecino q = (d + 3) mód 4.
                q = mod(d + 3, 4); 
                p_encontrado = 0; 
                image(pi_i, pi_j) = 5;
                
                % 5. A partir de q buscar en sentido antihorario el primer píxel con
                % valor 1, el cual es el nuevo elemento p i del contorno y actualizar
                % el valor de d con el código de su posición.                    
                for k = 0:3                     
                    switch mod(k+q,4)
                        case 0 % derecha
                            if image(pi_i, pi_j + 1) ~= 0                                        
                                p_encontrado = 1; 
                                pi_j = pi_j + 1;
                                d = 0;
                                break;
                            end
                        case 1 % arriba
                            if image(pi_i - 1, pi_j) ~= 0                                                  
                                p_encontrado = 1;
                                pi_i = pi_i - 1;
                                d = 1;
                                break;
                            end
                        case 2  % izquierda
                            if image(pi_i, pi_j - 1) ~= 0                    
                                p_encontrado = 1;
                                pi_j = pi_j - 1;
                                d = 2;       
                                break;
                            end
                        case 3 % abajo
                            if image(pi_i + 1, pi_j) ~= 0                    
                                p_encontrado = 1;
                                pi_i = pi_i + 1;
                                d = 3;
                                break;
                            end
                    end
                end
                
                % Si el píxel actual p i es igual al punto inicial del borde p 0 entonces
                % terminar el algoritmo; en caso contrario, regresar al Paso 4. 
                if i == pi_i && j == pi_j || p_encontrado == 0 
                    do = false;
                end
            end
%         end
%     end
% end
%          

image(image == 1) = 0;
imshow(image);



