function [bandwidth,angle,kappa,k] = get_arguments(num_img, total_img)
    
    % arguments
    arg = zeros(total_img, 4);
    % [bandwidth,angle,kappa,k]
    arg(1,:) = [1,4,3,4];
    arg(2,:) = [1.8,4,5,2];
    arg(3,:) = [.85,8,5,5];
    arg(4,:) = [1.2,4,4,5];
    arg(5,:) = [1.1,8,5,5];    
    arg(6,:) = [4,2,3,2];        
    arg(7,:) = [2.15,4,3,5];
    
    bandwidth = arg(num_img,1);
    angle = arg(num_img,2);
    kappa = arg(num_img,3);
    k = arg(num_img,4);
