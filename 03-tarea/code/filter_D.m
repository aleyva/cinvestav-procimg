% ideal lowpass filter
function D = filter_D(img)        
    
    % apply the fast fourier transform to img    
    [height, width] = size(img);    
    
    % Initialize filter.
    D = zeros(height,width);               
    
    % create the ideal lowpass filter
    for i = 1:height
        for j =1:width
            D(i,j) =  ( ((i-height/2)^2) + ((j-width/2)^2) ) ^.5;                      
%             if(ilpf(i,j) <= d0)
%                 ilpf(i,j) = 1;
%             end
        end
    end
end