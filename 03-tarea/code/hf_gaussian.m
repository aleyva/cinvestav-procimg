function filtered_img = hf_gaussian(img,D,d0,n,alpha,beta)
        
           
    im = double(img);

    %create the filter                
    H = (1 - exp(-D/d0).^(2*n) ); 
    H = alpha*(1-H)+beta;     
        
    % log of image
    im_l = log2(1+im);
    % DFT of logged image
    im_f = fft2(im_l);
    % Filter Applying DFT image
    im_nf = H.*im_f;
    % Inverse DFT of filtered image
    im_n = abs(ifft2(im_nf));
    % Inverse log 
    im_exp = exp(im_n); 
    filtered_img = im_exp/max(im_exp(:));   
end